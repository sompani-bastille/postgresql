CREATE USER "${pg_user}" WITH SUPERUSER PASSWORD '${pg_password}';
CREATE DATABASE "${pg_database}" WITH OWNER = "${pg_user}";
CREATE USER "sompani-readonly";
CREATE USER vc_readonly WITH NOLOGIN;
CREATE USER web_anon WITH NOLOGIN;
CREATE USER authenticator IN GROUP vc_readonly;
CREATE SCHEMA extensions;
CREATE EXTENSION dblink SCHEMA extensions;

CREATE EXTENSION pg_cron;
GRANT USAGE ON SCHEMA cron TO "${pg_user}";
GRANT EXECUTE ON FUNCTION dblink_connect_u(text) TO "${pg_user}";
GRANT EXECUTE ON FUNCTION dblink_connect_u(text, text) TO "${pg_user}";
CREATE OR REPLACE FUNCTION cron.sompani_schedule(job_name name, schedule text, command text, database_name text) RETURNS bigint
AS
$$
DECLARE
    job_id bigint;
BEGIN
    SELECT cron.schedule(job_name, schedule, command) INTO job_id;
    UPDATE cron.job SET database = database_name WHERE jobid = job_id;
    RETURN job_id;
END;
$$ LANGUAGE plpgsql;
